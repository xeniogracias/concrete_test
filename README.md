<<<<<<< HEAD
<<<<<<< HEAD
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
=======
# Webpack & Concrete5.8 Boilerplate ![CI status](https://img.shields.io/shippable/5444c5ecb904a4b21567b0ff/master.svg)

This is a new template for concrete5 projects that will be using webpack.

## Default Credentials

```
Username: admin
Password: n2RDQpq7
```

## Installation

### Requirements

- NPM (Latest stable version if possible) [Open Link](https://nodejs.org/en/)

### Setting Up the project

- Open terminal on root folder (htdocs/projectname) then run the following commands

```sh
chmod 755 run_setup.sh
chmod 755 run_dev.sh
chmod 755 run_build.sh
chmod 755 install_package.sh
chmod 755 uninstall_package.sh
```

- Open `run_setup.sh`, `run_dev.sh`, `run_build.sh`, `install_package.sh`, `uninstall_package.sh`
- Change `theme` to your `Project Name`

- Once you're done setting permissions for bash files, run the command below to install npm packages.

```sh
./run_setup.sh
```

##### Important Note: Do this step _ONCE_ OR when there are _NEW PACKAGES_

- Then, open `.gitignore`. Search for `application/themes/theme/node_modules`
- Rename the `theme` folder

##### Important Note: Do this step _ONCE_ - This will ignore all the contents of `node_modules`

## Running the Project

- Open terminal on root folder (htdocs/projectname) then run the following commands

```sh
./run_dev.sh
```

## Installing a package

- Open terminal on root folder (htdocs/projectname) then run the following commands

```sh
./install_package.sh <package_name> '--save' or '--save-dev'
```

## Uninstalling a package

- Open terminal on root folder (htdocs/projectname) then run the following commands

```sh
./uninstall_package.sh <package_name> '--save' or '--save-dev'
```

## Build the Project

- Open terminal on root folder (htdocs/projectname) then run the following commands

```sh
./run_build.sh
```

## Javascript Code Guidelines for ES6 & Above

[Open Link](https://github.com/airbnb/javascript)

##Theme Folder Structure

```bash
|-- theme
    |-- .eslintrc
    |-- .eslintignore
    |-- babel.config.js
    |-- webpack.config.js
    |-- package.json
    |-- node_modules
    |-- elements
    |-- dist
        |-- css
            |-- app.min.css
        |-- images
        |-- js
            |-- app.min.js
            |-- vendor.min.js
    |-- src
        |-- fonts
        |-- images
        |-- js
            |-- components
            |-- external
            |-- app.js
        |-- scss
            |-- base
            |-- components
            |-- elements
            |-- fonts
            |-- mixins
            |-- main.scss
```

## 2.0.0 Changes

- Remove eslint
- Change devtool of production mode from eval to cheap-module-source-map
- Change splitChunks from all to async and remove vendor.min.js from scripts.php
- Fix css loading from node_modules
- Fix svg on css for font and images
- Compress scss into 2 folders (mixins, base)

## How to import styles from external libraries

- Slick

```
import 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
```

- Swiper

```
import Swiper from 'swiper';
import 'swiper/dist/css/swiper.min.css';
```

## How to integrate react

```
./install_package.sh react --save-dev
./install_package.sh react-dom --save-dev
./install_package.sh @babel/preset-react --save-dev
```

# Webpack config

```
{
    test: /\.js$/,
    exclude: /(node_modules|bower_components)/,
    use: {
        loader: "babel-loader",
        options: {
            presets: ["@babel/preset-env", "@babel/preset-react"],
        }
    }
}
```
>>>>>>> initial commit
=======
>>>>>>> 3fef53b4ec344a81fae9bbce5a49dd71e37dd147
