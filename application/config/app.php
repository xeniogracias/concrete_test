<?php

return [
    'server_timezone'           => 'Asia/Dubai', /* Set time Zone here  if not set comment it out it will take the one set by default in cms dashboard settings.*/
    'canonical-url'             => '',
    'canonical-url-alternative' => '',
    'theme_paths'               => array(
        '/login'          => 'theme',
        '/page_not_found' => 'theme',
    )
];