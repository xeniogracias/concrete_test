<?php
defined('C5_EXECUTE') or die("Access Denied.");
?>

<div class="page-padding-top page-min-height">


    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title">
                    <?php $a = new \Concrete\Core\Area\Area('Title'); $a->display($c)?>
                </div>
            </div>
        </div>
        <div class="page-divider"></div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-section">
                    <?php $a = new \Concrete\Core\Area\Area('Description'); $a->display($c)?>
                </div>
            </div>
        </div>
    </div>
</div>