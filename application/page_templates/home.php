<?php
defined('C5_EXECUTE') or die("Access Denied.");
$imageHelper = Core::make('helper/image');

$themePath = $this->getThemePath();

$file = $c->getAttribute('banner_image');
$thumb = null;
$imgUrl = null;
if($file) {
    $thumb = $imageHelper->getThumbnail($file, 2400, 1200);
}
if($thumb) {
    $imgUrl = $thumb->src;
}
?>

<div class="page-padding-top">
    <?php if($imgUrl) { ?>
        <div class="page-bg-fixed" style="background-image: url('<?php echo $imgUrl?>')"> </div>
    <?php } ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title">
                    <?php $a = new \Concrete\Core\Area\Area('Title'); $a->display($c)?>
                </div>
            </div>
        </div>
        <div class="page-divider"></div>
    </div>
</div>