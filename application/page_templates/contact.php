<?php
defined('C5_EXECUTE') or die("Access Denied.");
$imageHelper = Core::make('helper/image');


$file = $c->getAttribute('banner_image');
$thumb = null;
$imgUrl = null;
if($file) {
    $thumb = $imageHelper->getThumbnail($file, 2400, 1200);
}
if($thumb) {
    $imgUrl = $thumb->src;
}
?>

<div class="page-padding-top" data-aos="fade-in" data-aos-delay="300" data-aos-duration="1000">
    <?php if($imgUrl) { ?>
        <div data-aos="bg-anim" data-aos-delay="300" data-aos-duration="1500" class="page-bg-fixed" style="background-image: url('<?php echo $imgUrl?>')"> </div>
    <?php } ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title blink">
                    <?php $a = new \Concrete\Core\Area\Area('Title'); $a->display($c)?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="page-divider separator"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="map-section">
                    <?php $a = new \Concrete\Core\Area\Area('Map'); $a->display($c)?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="page-divider separator"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-section" data-aos="fade-in" data-aos-delay="300" data-aos-duration="1000">
                    <?php $a = new \Concrete\Core\Area\Area('Contact Form'); $a->display($c)?>
                </div>
            </div>
        </div>
    </div>
</div>